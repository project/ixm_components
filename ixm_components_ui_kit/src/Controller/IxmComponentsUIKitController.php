<?php
namespace Drupal\ixm_components_ui_kit\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * UI Kit Controller.
 */
class IxmComponentsUIKitController extends ControllerBase {

  /**
   * Returns the user interface kit designed by IXM.
   *
   * @return array
   *   A simple renderable array.
   */
  public function content() {
    return array (
      '#theme' => [
        'ixm_components_ui_kit',
      ],
    );
  }
}
